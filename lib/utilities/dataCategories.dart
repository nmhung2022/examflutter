import 'package:news/models/category.dart';
import 'package:news/models/category.dart';

List<Category> getCategories() {
  List<Category> myCategories = List<Category>();
  Category category;

  category = new Category();
  category.categoryName = "Sports";
  category.imageAssetUrl =
      "https://images.unsplash.com/photo-1474546652694-a33dd8161d66?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Nnx8c3BvcnR8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60";
  myCategories.add(category);

  category = new Category();
  category.categoryName = "Business";
  category.imageAssetUrl =
      "https://images.unsplash.com/photo-1536329583941-14287ec6fc4e?ixid=MnwxMjA3fDF8MHxzZWFyY2h8MXx8YnVzaW5lc3N8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60";
  myCategories.add(category);

  category = new Category();
  category.categoryName = "Business IT Analysts";
  category.imageAssetUrl =
      "https://media.istockphoto.com/photos/analyst-working-with-business-analytics-and-data-management-system-on-picture-id1286642964?b=1&k=20&m=1286642964&s=170667a&w=0&h=doK0J0FhFqTF83bb5XggguZgbR-pF16ngrDcr7xG21o=";
  myCategories.add(category);

  category = new Category();
  category.categoryName = "Computer Games";
  category.imageAssetUrl =
      "https://images.unsplash.com/photo-1614294149010-950b698f72c0?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8Y29tcHV0ZXIlMjBnYW1lfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60";
  myCategories.add(category);

  category = new Category();
  category.categoryName = "Movies";
  category.imageAssetUrl =
      "https://images.unsplash.com/photo-1485846234645-a62644f84728?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8bW92aWV8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60";
  myCategories.add(category);

  category = new Category();
  category.categoryName = "Music";
  category.imageAssetUrl =
      "https://khodohoa.vn/wp-content/uploads/2018/02/music-07-300x188.jpg";
  myCategories.add(category);

  return myCategories;
}
